new WOW().init();

//Анимация с набором телефонного номера

jQuery(document).ready(function() {

    var fieldValue ='';
    $(".form-item").focusin(function(){
        fieldValue = $(this).val();
        $(this).val('');
    });
    $(".form-item").focusout(function(){
        $(this).val(fieldValue);
    });
    
    //анимация набора номера телефона

    var digits = $('#phone-number').find('li');
    var digitsLen = digits.length;

    for(var i = 0; i < digitsLen; i++)
    {
        var item = $(digits[i]).find('p').attr('id', 'dig' + i);
    }

    var animArray = new Array(); //сюда в виде строк кладется анимация

    //сборка строки анимации
    function extrAnimArray(i)
    {
        var s;
        if(i > animArray.length - 1) 
        { 
            return " );"; 
        }
        else
        {
            if(i == 0)
            {
                s = animArray[i] + extrAnimArray(i + 1);
            }
            else 
            {
                s = ", function(){" + animArray[i] + extrAnimArray(i + 1) + "});"
            }
        }
        return s;
    }

    function createAnimation(k)
    {
        for(var i = 0; i < k; i++)
        {
            s = "$(\"#dig" + String(i) + "\").animate({\"font-size\":" + "\"4em\"}" ;
            animArray.push(s); //поместим строку в массив
        }

        st = extrAnimArray(0);
        alert(st);
        setTimeout(st, 1000);
    }

    //createAnimation(digitsLen);

    //alert("weeerr");
    /*
    setInterval(function(){
        $('#dig0').animate({"font-size": "4em"}, function() {
            $('#dig0').animate({"font-size": "3em"});
            $('#dig1').animate({"font-size": "4em"}, function() {
                $('#dig1').animate({"font-size": "3em"}).stop();
                $('#dig2').animate({"font-size": "4em"}, function(){
                    $('#dig2').animate({"font-size": "3em"}).stop();
                    $('#dig3').animate({"font-size": "4em"}, function(){
                        $('#dig3').animate({"font-size": "3em"});
                        $('#dig4').animate({"font-size": "4em"}, function(){
                            $('#dig4').animate({"font-size": "3em"});
                            $('#dig5').animate({"font-size": "4em"}, function(){
                                $('#dig5').animate({"font-size": "3em"});
                                $('#dig6').animate({"font-size": "4em"}, function(){
                                    $('#dig6').animate({"font-size": "3em"});
                                    $('#dig7').animate({"font-size": "4em"}, function(){
                                        $('#dig7').animate({"font-size": "3em"});
                                        $('#dig8').animate({"font-size": "4em"}, function(){
                                            $('#dig8').animate({"font-size": "3em"});
                                            $('#dig9').animate({"font-size": "4em"}, function(){
                                                $('#dig9').animate({"font-size": "3em"});
                                                $('#dig10').animate({"font-size": "4em"}, function(){
                                                    $('#dig10').animate({"font-size": "3em"});
                                                    $('#dig11').animate({"font-size": "4em"}, function(){
                                                        $('#dig11').animate({"font-size": "3em"});
                                                        $('#dig12').animate({"font-size": "4em"}, function(){
                                                            $('#dig12').animate({"font-size": "3em"});
                                                            $('#dig13').animate({"font-size": "4em"});
                                                            $('#dig13').animate({"font-size": "3em"});
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }, 1000);
*/
    //alert("567890");                 

    /*setInterval(function(){
        $('#dig0').animate({"font-size": "4em"}, function() {
            $('#dig0').animate({"font-size": "2em"}).stop();
            $('#dig1').animate({"font-size": "4em"});
        });
    
        $('#dig1').animate({"font-size": "2em"}, function() {
            $('#dig2').animate({"font-size": "4em"});
            $('#dig2').animate({"font-size": "2em"});
        });
        
    }, 1000);*/

    //слайдер для мобильного приложения

    $(function() 
    {
        var width=$('.slider-box').width();                     
        var interval = 4000;                                    

        $('.slider img:last').clone().prependTo('.slider');     
        $('.slider img').eq(1).clone().appendTo('.slider');     
        $('.slider').css('margin-left', -width);   
                     
        setInterval(function(){
            var margin = parseInt($('.slider').css('marginLeft'));  
            var width=$('.slider-box').width();                    
            var slidersAmount=$('.slider').children().length;      
        
            if(margin!=(-width*(slidersAmount-1)))                 
            {
                margin=margin-width;                                
            }else
            {                                                  
                $('.slider').css('margin-left', -width);           
                margin=-width*2;
            }
            $('.slider').animate({marginLeft:margin}, 2000);
        },interval);                    
    });

//слайдер для комментариев клиентов
    $(function() 
    {
        var width=$('.slider-box__happy-clients').width(); 
        var moveStep = $('.client-feedback').width() + parseInt($('.client-feedback').css('marginRight')); 
        //alert("moveStep = " + moveStep);                   
        var interval = 4000;                                   

        $('.client-feedback:last').clone().prependTo('.happy-clients-container');     
        $('.client-feedback').eq(1).clone().appendTo('.happy-clients-container');     
        $('.happy-clients-container').css('margin-left', -moveStep);   
                     
        setInterval(function(){
            var margin = parseInt($('.happy-clients-container').css('marginLeft')); 
            //alert("margin = " + margin); 
            var moveStep = $('.client-feedback').width() + parseInt($('.client-feedback').css('marginRight')); 
            var width=$('.slider-box__happy-clients').width();                    
            var slidersAmount=$('.happy-clients-container').find('.client-feedback').length; 
            //alert("slidersAmount = " + slidersAmount);   
        
            if(margin != (-moveStep * (slidersAmount - 1)))                  
            {
                margin -= moveStep; 
            }
            else
            {                                       
                $('.happy-clients-container').css('margin-left', -moveStep); 
                margin = -moveStep * 2;
            }
            $('.happy-clients-container').animate({marginLeft:margin}, 3000);
        },interval);                   
    });
    
    
});
